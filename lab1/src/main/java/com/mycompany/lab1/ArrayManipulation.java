/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab1;

import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author asus
 */
public class ArrayManipulation {
    public static void main(String[] args) throws Exception {
        Scanner kb = new Scanner(System.in);
        int[] number = {5,8,3,2,7};
        String[] name = {"Alice","Bob","Charlie","David"};
        double[] values = new double[4];
        for(int i=0;i<number.length;i++){
            System.out.printf(number[i]+" ");
        }
        System.out.println();

        for (String name1 : name) {
            System.out.println(name1);
        }

        for(int i=0;i<values.length;i++){
            values[i] = kb.nextDouble();
            System.out.println(values[i]);
        }

        int sum = 0;
        for(int i=0;i<number.length;i++){
            
            sum += i;   
        }System.out.println("sum of array in number = "+sum);

        double max = 0.0;
        for(int i=0;i<values.length;i++){
            if(max <= i){
                max = values[i];       
        }System.out.println("maxmium of values = "+max);
    }
    
        String[] reversedNames = new String[name.length];
        for(int i=0;i<name.length;i++){
            reversedNames[i] = name[name.length-1 -i];
        }
        for (String reversedName : reversedNames) {
            System.out.print(Arrays.toString(reversedNames));
        }
    }
}
